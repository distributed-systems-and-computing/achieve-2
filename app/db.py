import sqlalchemy 
import os
from sys import exit
import time

env = {
    'POSTGRES_USER': None,
    'POSTGRES_PASSWORD': None,
    'POSTGRES_HOST': None,
    'POSTGRES_PORT': None,
    'POSTGRES_DB': None
}

for key in env.keys():
    env[key] = os.getenv(key)


if any(value is None for key, value in env.items()):
    print("Can't get some variable from envirement:")
    for key, value in env.items():
        print(key, value)
    exit(1)

db_string = f"postgres://{env['POSTGRES_USER']}:{env['POSTGRES_PASSWORD']}" \
            f"@{env['POSTGRES_HOST']}:{env['POSTGRES_PORT']}/{env['POSTGRES_DB']}"
db = sqlalchemy.create_engine(db_string)



def execute_query(query):
    cnt = 0
    while True:
        try:
            return db.execute(query)
        except sqlalchemy.exc.OperationalError as e:
            try:
                ans = db.execute(query)
                print('DB connected', flush=True)
                return ans
            except sqlalchemy.exc.OperationalError as e:
                cnt += 1
                if cnt > 5:
                    print('DB connection lost', flush=True)
                    exit(1)
                print('Wait for db ', db_string, flush=True)
                time.sleep(5)

execute_query('CREATE TABLE IF NOT EXISTS numbers (num integer PRIMARY KEY)')  

def clear_db():
    execute_query('delete from numbers')

def insert_num(num):
    try:
        execute_query(f'INSERT INTO numbers (num) values ({num})')
    except sqlalchemy.exc.IntegrityError as e:
        return False
    return True

def get_num(num):
    result_set = execute_query(f'SELECT num from numbers WHERE numbers.num={num} or numbers.num={num+1}')
    res = [r[0] for r in result_set]
    res.sort()
    return res

def get_all_num():
    result_set = execute_query('SELECT * FROM numbers')  
    res = [r[0] for r in result_set]
    res.sort()
    return res
